
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: 'dashboard', component: () => import('pages/Index.vue') },
      { path: 'student', component: () => import('pages/studinfo.vue') },
      { path: 'courses', component: () => import('pages/courses.vue') },
      { path: 'test', component: () => import('pages/test.vue') },
      { path: 'logout', component: () => import('pages/logout.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
