import axios from 'axios'
// import api from '../config/api'
let api = 'http://localhost:3000'
export default {
  create (data) {
    return axios.request({
      url: api + '/student/',
      method: 'post',
      data: data
    })
  },
  get (id) {
    return axios.request({
      url: api + '/student/' + id,
      method: 'get'
    })
  },
  update (data, id) {
    return axios.request({
      url: api + '/student/' + id,
      method: 'put',
      data: data
    })
  },
  search (data) {
    return axios.request({
      url: api + '/item?name_like=' + data,
      method: 'get'
    })
  },
  delete (id) {
    return axios.request({
      url: api + '/student/' + id,
      method: 'delete'
    })
  }
}
