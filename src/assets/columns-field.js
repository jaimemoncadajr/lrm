export default [
  {
    name: 'first_name',
    required: true,
    label: 'First Name',
    align: 'left',
    field: 'first_name',
    sortable: true,
    classes: 'my-class',
    style: 'width: 500px'
  },
  {
    name: 'last_name',
    required: true,
    label: 'Last Name',
    align: 'left',
    field: 'last_name',
    sortable: true,
    classes: 'my-class',
    style: 'width: 500px'
  },
  {
    name: 'middle_name',
    required: true,
    label: 'Middle Name',
    align: 'left',
    field: 'middle_name',
    sortable: true,
    classes: 'my-class',
    style: 'width: 500px'
  },
  {
    name: 'age',
    required: true,
    label: 'Age',
    align: 'left',
    field: 'age',
    sortable: true,
    classes: 'my-class',
    style: 'width: 100px'
  },
  {
    name: 'address',
    required: true,
    label: 'Address',
    align: 'left',
    field: 'address',
    sortable: true,
    classes: 'my-class',
    style: 'width: 1000px'
  },
  {
    name: 'advicer',
    required: true,
    label: 'Advicer',
    align: 'left',
    field: 'advicer',
    sortable: true,
    classes: 'my-class',
    style: 'width: 500px'
  },
  {
    name: 'department',
    required: true,
    label: 'Department',
    align: 'left',
    field: 'department',
    sortable: true,
    classes: 'my-class',
    style: 'width: 500px'
  },
  {
    name: 'level',
    required: true,
    label: 'Level',
    align: 'left',
    field: 'level',
    sortable: true,
    classes: 'my-class',
    style: 'width: 100px'
  },
  {
    name: 'edit',
    required: true,
    label: 'Edit',
    align: 'left',
    field: 'edit',
    sortable: true,
    classes: 'my-class',
    style: 'width: 50px'
  },
  {
    name: 'delete',
    required: true,
    label: 'Delete',
    align: 'left',
    field: 'delete',
    sortable: true,
    classes: 'my-class',
    style: 'width: 500px'
  }
]
